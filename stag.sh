string=$STAG_DEPLOY_SERVER
set -f
array=(${string//:/ })

for i in "${!array[@]}"; do
    echo "Deploying on server ${!array[i]}"
done
